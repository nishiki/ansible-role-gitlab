# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## Unreleased

### Added

- feat: add gitlab_registry_url variable
- feat: add options for backup
- test: add support debian 12

### Changed

- test: use personal docker registry

### Removed

- test: remove support debian 10
- test: remove support debian 11

## v1.1.0 - 2021-08-24

### Added

- feat: add gitlab_data_dir variable
- feat: add gitlab_pages_url variable
- feat: add complex variables in config file
- feat: add backup cron
- test: add support debian 11

### Changed

- chore: use FQCN for module name

## v1.0.0 - 2020-04-11

- first version
