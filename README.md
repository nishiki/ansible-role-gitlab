# Ansible role: Gitlab

[![Version](https://img.shields.io/badge/latest_version-1.1.0-green.svg)](https://code.waks.be/nishiki/ansible-role-gitlab/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-gitlab/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-gitlab/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-gitlab/actions?workflow=molecule.yml)

Install and configure a Gitlab server

## Requirements

- Ansible >= 2.9
- Debian
  - Bullseye
  - Bookworm

## Role variables

- `gitlab_url` - set the gitlab url
- `gitlab_pages_url` - set the gitlab pages url
- `gitlab_data_dir` - set the path for repository
- `gitlab_config` - set the config

```
  grafana:
    enabled: false
  gitlab_rails:
    backup_keep_time: 604800
    backup_archive_permissions: 0644
```

- `gitlab_backup_cron_month` - set the month for the backup cron (default: `'*'`)
- `gitlab_backup_cron_weekday` - set the weekday for the backup cron (default: `'*'`)
- `gitlab_backup_cron_day` - set the day for the backup cron (default: `'*'`)
- `gitlab_backup_cron_hour` - set the hour for the backup cron (default: `2`)
- `gitlab_backup_cron_minute` - set the minute for the backup cron (default: `30`)
- `gitlab_backup_skip` - list with [skip data](https://docs.gitlab.com/ee/administration/backup_restore/backup_gitlab.html#excluding-specific-data-from-the-backup)
- `gitlab_backup_auto` - set if a backup if create during update (default: `true`)

## How to use

```
- hosts: server
  roles:
    - gitlab
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule molecule-docker docker ansible-lint pytest-testinfra yamllint`
- run `molecule test`

## License

```
Copyright (c) 2020 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
