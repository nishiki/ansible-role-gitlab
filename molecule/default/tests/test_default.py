import testinfra.utils.ansible_runner

def test_packages(host):
  for package_name in ['cron', 'gitlab-ce']:
    package = host.package(package_name)
    assert package.is_installed

def test_git_directory(host):
  path = host.file('/opt/data/git')
  assert path.exists
  assert path.is_directory

def test_config_file(host):
  config = host.file('/etc/gitlab/gitlab.rb')
  assert config.exists
  assert config.is_file
  assert config.user == 'root'
  assert config.group == 'root'
  assert config.mode == 0o600
  assert config.contains("external_url 'http://gitlab.example.com'")
  assert config.contains("prometheus\\['enable'\\] = false")

def test_skip_auto_backup_file(host):
  config = host.file('/etc/gitlab/skip-auto-backup')
  assert config.exists
  assert config.is_file
  assert config.user == 'root'
  assert config.group == 'root'
  assert config.mode == 0o644

def test_service(host):
  service = host.service('gitlab-runsvdir')
  assert service.is_running
  assert service.is_enabled

def test_socket(host):
  socket = host.socket('tcp://0.0.0.0:80')
  assert socket.is_listening

def test_grafana_down(host):
  try:
    host.process.get(user='gitlab-prometheus', comm='grafana-server')
    assert False
  except:
    assert True

def test_cron_file(host):
  config = host.file('/var/spool/cron/crontabs/root')
  assert config.exists
  assert config.is_file
  assert config.contains('30 2 \\* \\* \\* gitlab-backup create SKIP=db,registry')
